CC=gcc
ARGS=-g

all: main.o
	${CC} ${ARGS} -o main main.o

main.o: main.c
	${CC} -g -c main.c

clean:
	rm main *.o
